//TODO:
//1. Needs to detect current desktop resolution to launch at full screen
//2. UI for res settings

#ifndef WINDOW
#define WINDOW

#include "filesystem.h"
#include "mains.h"
#include "motor.h"

int w_height;
int w_width;
bool Windowed;
bool WindowErrorCheck;
bool WindowError();
void Debug();
void ToggleFullScreen();
void DestWindow();
void DestRenderer();
void NumDisplay();
SDL_Window *window;
SDL_Renderer *w_renderer;
Uint32 w_windowflag;
Uint32 WindowFlags();

SDL_Renderer *GetRenderer() {return w_renderer;}

void NumDisplay()
{
    SDL_DisplayMode current;
    int i;
    for (i = 0; i < SDL_GetNumVideoDisplays(); i++) {
        int a = SDL_GetCurrentDisplayMode(i, &current);

        if (a != 0) {
            std::cout << i << SDL_GetError();
        }
        else {
            std::cout << i
                << current.w << " x "
                << current.h << " @ "
                << current.refresh_rate << " hz\n";
        }
    }
    if (SDL_GetNumVideoDisplays <= 0) {
        std::cout << "No displays available\n";
        SDLErrorPrint();
    }
}

void DestRenderer()
{
    std::cout << "[Cleanup Started]\nRenderer destroyed\n";
    SDL_DestroyRenderer(w_renderer);
}

void DestWindow()
{
    std::cout << "Window destroyed\n";
    SDL_DestroyWindow(window);
}

static int CreateWindow(int w_width, int w_height)
{
    SDL_GetNumVideoDisplays();
    SDL_GetWindowDisplayIndex(window);

    window = SDL_CreateWindow(
            "Motor Test",
            SDL_WINDOWPOS_UNDEFINED,
            SDL_WINDOWPOS_UNDEFINED,
            w_width,
            w_height,
            SDL_WINDOW_SHOWN);

    w_renderer = SDL_CreateRenderer (window, 0, SDL_RENDERER_ACCELERATED);
    std::cout << "Window created\n";
    NumDisplay();
    return 0;
}

static void SetWindowIcon(const std::string Icon)
{
    SDL_Surface *IconSurface = IMG_Load(Icon.c_str());
    SDL_SetWindowIcon(window, IconSurface);
    SDL_FreeSurface(IconSurface);
}
#endif
