#ifndef MOTORMAINS
#define MOTORMAINS

#include "filesystem.h"
#include "globaltimer.h"
#include "glob.h"
#include "input.h"
#include "window.h"
#include "assets.h"
#include "text.h"
#include "parser.h"
#include "init.h"
#include "player.h"
#include "entity.h"

#endif
