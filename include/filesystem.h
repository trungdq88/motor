#ifndef FILESYSTEM
#define FILESYSTEM

#include "mains.h"
#include "motor.h"

class Filesystem
{
    public:
        const std::string Archive;
        const std::string Dir;
        const std::string FChck;
        char *Data;
        int Priority;
        PHYSFS_sint64 Size;
        PHYSFS_file *FS_File;
    public:
        Filesystem();
        ~Filesystem();
        int SetArchive(const std::string Archive);
        int FileCheck(const std::string FChck);
        int CloseFile();
        const char* OpenFile(const std::string PhysFile);
};

Filesystem::Filesystem()
{

}

Filesystem::~Filesystem()
{

}

// Sets which archive for PhysFS to open
// @Archive archive to be opened
int Filesystem::SetArchive(const std::string Archive)
{
    PHYSFS_mount(Archive.c_str(), NULL, 1);

    if (PHYSFS_mount(Archive.c_str(), NULL, 1) != 0) {
        std::cout << "Archive " << Archive << " added/exist\n";
    }
    else if (PHYSFS_mount(Archive.c_str(), NULL, 1) == 0) {
        std::cout << "Archive " << Archive << " not added/doesn't exist\t"
            << "PhysFS Error: " << PHYSFS_getLastError() << "\n";
    }
    return 0;
}

// Checks for file existance in archive
// @FChck file to be checked
int Filesystem::FileCheck(const std::string FChck)
{
    PHYSFS_exists(FChck.c_str());

    if (PHYSFS_exists(FChck.c_str()) != 0) {
        std::cout << "File exists in archive " << Dir << "\n";
        return 0;
    }
    else if (PHYSFS_exists(FChck.c_str()) == 0) {
        std::cout << "File " << FChck
            << "doesn't exist\t"
            << "PhysFS Error: " <<PHYSFS_getLastError() << "\n";
        return -1;
    }
}

// Opens file in archive
// @PhysFile file to be opened
const char * Filesystem::OpenFile(const std::string PhysFile)
{
    if (PHYSFS_openRead(PhysFile.c_str()) == NULL) {
        std::cout << "File " << PhysFile << " cannot be opened\n";
    }
    else if (PHYSFS_openRead(PhysFile.c_str()) != NULL) {
        std::cout << "File " << PhysFile << " opened\n";
        FS_File = PHYSFS_openRead(PhysFile.c_str());
        Size = PHYSFS_fileLength(FS_File);
        Data = new char[PHYSFS_fileLength(FS_File)];
        PHYSFS_read(FS_File, Data, 1, Size);
        return PhysFile.c_str();
    }
}

int Filesystem::CloseFile()
{
    if (FS_File != NULL) {
        delete[] Data;
        return 0;
    }
}

#endif
