#ifndef TEXT
#define TEXT

#include "mains.h"
#include "motor.h"

class Text : public Filesystem
{
    public:
        SDL_Texture *FontOpt;
        SDL_Surface *FontSurface;
        SDL_Color FontColor;
        TTF_Font *T_Font;
        const std::string Font;
        const std::string PrintText;
        int fR, fG, fB;
        int FontHeight;
    public:
        Text();
        ~Text();
        void DrawText();
        void DestFont();
        int LoadText(const std::string PrintText, int fR, int fG, int fB);
        int SetFont(int FontHeight, const std::string Font);
};

Text::Text()
{

}

Text::~Text()
{
    DestFont();
}

int Text::SetFont(int FontHeight, const std::string Font)
{
    T_Font = TTF_OpenFont(Font.c_str(), FontHeight);
}

int Text::LoadText(const std::string PrintText, int fR, int fG, int fB)
{
    SDL_Color FontColor={fR, fG, fB};
    FontSurface = TTF_RenderText_Solid(T_Font, PrintText.c_str(), FontColor);
    FontOpt = SDL_CreateTextureFromSurface(GetRenderer(), FontSurface);

    if (FontSurface != NULL) {
        SDL_FreeSurface(FontSurface);
    }
    if (T_Font != NULL) {
        TTF_CloseFont(T_Font);
    }
}

void Text::DrawText()
{
    SDL_RenderCopy(GetRenderer(), FontOpt, NULL, NULL);
    SDL_RenderPresent(GetRenderer());
}

void Text::DestFont()
{
    if (FontOpt != NULL) {
        SDL_DestroyTexture(FontOpt);
        std::cout << "Font " << Font << " destroyed\n";
    }
    else if (FontOpt == NULL) {
        std::cout << "No font to destroy\n";
    }
}
#endif
