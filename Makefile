CXX=g++
CF=-Wwrite-strings -fprofile-arcs -ftest-coverage
LSET=-lSDL2 -lSDL2_image -lSDL2_ttf -lpthread -lyaml -lphysfs
LDIR=-L/usr/lib
INCSDL=-I/usr/include/SDL2
INCYML=-I/usr/include/
INCMTR=-Isrc/
BIN=MotorTest
TEST=TimedMotorTest
LOG=log.txt

MotorTest:
	$(CXX) $(CF) $(INCSDL) $(INCYML) $(INCMTR) -o $(BIN) src/testbed.cpp $(LDIR) $(LSET)
	@echo Build done

clean:
	rm -f $(BIN)
	./bin/clean
	@echo Clean done

test:
	$(CXX) $(CF) $(INCSDL) $(INCYML) $(INCMTR) -o $(TEST) src/timedtestbed.cpp $(LDIR) $(LSET)
	@echo Timed test build done

cleantest:
	rm -f $(TEST)
	./bin/clean
	@echo Test clean done
